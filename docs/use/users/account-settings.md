# Your account settings

You can access to your account settings by clicking your avatar on the top bar and **My account**.

## General

### Email

You can change the email you use to connect to your account. To do so, you have to:

  1. click your avatar on the top bar
  * click **My account**
  * click **General** tab on left sidebar
  * enter your new email in **New email** field
  * enter your password in **Password** field
  * click **Change my email** button.

!!! note
    You'll receive a confirmation email.

### Password

To change your password, you have to:

  1. click your avatar on the top bar
  * click **My account**
  * click **General** tab on left sidebar
  * enter your current one into **Old password** field
  * enter your new one into **New password** field
  * click **Change my password** button.

### Delete account

!!! danger
    You'll lose everything. Identities, settings, events created, messages and participations will be gone forever.
    **There will be no way to recover your data**.

To delete your account you have to:

  1. click your avatar on the top bar
  * click **My account**
  * click **General** tab on left sidebar
  * click **Delete my account** button
  * enter your password to confirm your action
  * click **Delete everything** button.

## Preferences

You can choose the language of your Mobilizon interface and your timezone. To do so, you have to:

1. click your avatar on the top bar
* click **My account**
* click **Preferences** tab on left sidebar
* change the language or/and the timezone.

## Email notifications

### Participation notifications

You can choose when you want to be notify by email.To do so, you have to:

1. click your avatar on the top bar
* click **My account**
* click **Email notifications** tab on left sidebar
* choose between:
  * Notification on the day of the event
  * Recap every week
  * Notification before the event

### Organizer notifications

As an organizer you can receive notifications for manually approved participations to an event. You can choose between:

  * Do not receive any mail
  * Receive one email per request
  * Hourly email summary
  * Daily email summary
