# Comment an event

When comments are allowed for an event, you can add one by using comments section:

![comments section](../../images/en/rose-comment-event-EN.png)

You also can reply to a specific comment by clicking **Reply** link:

![reply comments section](../../images/en/comments-replies-event.png)
