# Group ressources

 A place to store links to documents or resources of any type.

 **Ressources** section allows you to add:

  * folder
  * web links
  * calc
  * pad
  * visioconference

![image add resources](../../images/en/group-add-ressources.png)
